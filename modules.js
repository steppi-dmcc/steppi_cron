const path = require('path');

const envPath = path.join(__dirname, '.env');
require('dotenv').config({ path: envPath });

module.exports = [{
  packagePath: './modules/core',
  port: process.env.PORT,
  env: process.env.NODE_ENV,
  url: process.env.DNS,
}, {
  packagePath: './modules/db',
  environment: process.env.NODE_ENV,
}, {
  packagePath: './modules/helpers',
  jwtSecret: process.env.JWTSECRET,
  env: process.env.NODE_ENV,
  saltRound: process.env.SALTROUND,
  azureStorage: process.env.AZURE_STORAGE_ACCOUNT,
  azureStorageAccessKey: process.env.AZURE_STORAGE_ACCESS_KEY,
  azureStorageContainer: process.env.AZURE_STORAGE_BLOB_CONTAINER || 'dev-bucket',
  azureStorageUrl: process.env.AZURE_STORAGE_URL,
  serverUrl: process.env.DNS,
  iosAppUrl: process.env.IOS_APP_STORE_URL,
  androidAppUrl: process.env.PLAYSTORE_URL,
  firebaseApiKey: process.env.FIREBASE_API_KEY,
  iosAppBundle: process.env.IOS_APP_BUNDLE,
  iosStoreId: process.env.IOS_STORE_ID,
  androidAppBundle: process.env.ANDROID_APP_BUNDLE,
  dynamicLinkURL: process.env.FIREBASE_DYNAMIC_LINK,
  spareDynamicLink: process.env.SPARE_DYNAMIC_LINK,
  appLinkUrl: process.env.APP_LINK_URL,
}];

