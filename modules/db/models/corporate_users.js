const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const CorporateUsers = sequelize.define('corporate_users', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    corporationId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    isVerified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
    indexes: [
      { fields: ['userId', 'corporationId'], unique: true },
    ],
  });
  CorporateUsers.associate = (models) => {
    CorporateUsers.belongsTo(models.corporations, {
      foreignKey: 'corporationId',
      as: 'corporation',
      onDelete: 'CASCADE',
    });
    CorporateUsers.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
  };
  CorporateUsers.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };

  return CorporateUsers;
};
