const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ReferralCodes = sequelize.define('referral_codes', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      unique: true,
    },
    referralCode: {
      type: DataTypes.STRING(8),
      allowNull: false,
      unique: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ReferralCodes.associate = (models) => {
    ReferralCodes.hasMany(models.users, {
      foreignKey: 'referredBy',
      as: 'referredUsers',
    });
    ReferralCodes.belongsTo(models.users, {
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      as: 'user',
    });
  };
  ReferralCodes.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };

  return ReferralCodes;
};
