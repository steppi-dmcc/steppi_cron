const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const UserMerchantFavorites = sequelize.define('user_merchant_favorites', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    merchantId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  UserMerchantFavorites.associate = (models) => {
    UserMerchantFavorites.belongsTo(models.merchants, {
      foreignKey: 'merchantId',
      as: 'merchant',
      onDelete: 'CASCADE',
    });
    UserMerchantFavorites.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
  };
  UserMerchantFavorites.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return UserMerchantFavorites;
};
