const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const RewardRedeemCodes = sequelize.define('reward_redeem_codes', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    redeemCode: {
      type: DataTypes.STRING(150),
      allowNull: false,
    },
    rewardId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  RewardRedeemCodes.associate = (models) => {
    RewardRedeemCodes.belongsTo(models.rewards, {
      foreignKey: 'rewardId',
      as: 'reward',
      onDelete: 'CASCADE',
    });
  };
  RewardRedeemCodes.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return RewardRedeemCodes;
};
