const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const TeamDiscussions = sequelize.define('team_discussions', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    teamId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    message: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    repliedTo: DataTypes.UUID,
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  TeamDiscussions.associate = (models) => {
    TeamDiscussions.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
    TeamDiscussions.belongsTo(models.team_discussions, {
      foreignKey: 'repliedTo',
      as: 'parent',
      onDelete: 'CASCADE',
    });
    TeamDiscussions.hasMany(models.team_discussions, {
      foreignKey: 'repliedTo',
      as: 'replies',
    });
  };
  TeamDiscussions.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };

  return TeamDiscussions;
};
