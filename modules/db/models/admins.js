const omit = require('lodash/omit');
const { hashSync, genSaltSync } = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const admin = sequelize.define('admins', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    picture: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  admin.associate = (models) => {
    admin.hasMany(models.tags, {
      foreignKey: 'createdBy',
      as: 'tags',
    });
  };
  admin.prototype.safeModel = function safeModel() {
    return omit(this.toJSON(), ['password']);
  };
  admin.prototype.securePassword = function () {
    return hashSync(this.dataValues.password, genSaltSync(10));
  };
  return admin;
};
