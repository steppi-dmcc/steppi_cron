const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const UserNotifications = sequelize.define('user_notifications', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    relatedRewardId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    relatedChallengeId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    relatedCorporateId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    relatedTeamId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    relatedMessageId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    relatedToughMudderId: DataTypes.UUID,
    relatedToughMudderSubId: DataTypes.UUID,
    relatedUserId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    isRead: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isReadHome: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isHome: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isSent: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  UserNotifications.associate = (models) => {
    UserNotifications.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
    UserNotifications.belongsTo(models.challenges, {
      foreignKey: 'relatedChallengeId',
      as: 'challenge',
      onDelete: 'CASCADE',
    });
    UserNotifications.belongsTo(models.corporations, {
      foreignKey: 'relatedCorporateId',
      as: 'corporation',
      onDelete: 'CASCADE',
    });
    UserNotifications.belongsTo(models.challenge_teams, {
      foreignKey: 'relatedTeamId',
      as: 'team',
      onDelete: 'CASCADE',
    });
    UserNotifications.belongsTo(models.team_discussions, {
      foreignKey: 'relatedMessageId',
      as: 'message',
      onDelete: 'CASCADE',
    });
    UserNotifications.belongsTo(models.rewards, {
      foreignKey: 'relatedRewardId',
      as: 'reward',
      onDelete: 'CASCADE',
    });
    UserNotifications.belongsTo(models.users, {
      foreignKey: 'relatedUserId',
      as: 'from',
      onDelete: 'CASCADE',
    });
  };
  UserNotifications.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return UserNotifications;
};
