module.exports = (sequelize, DataTypes) => {
  const UserToughMudderChallenges = sequelize.define('user_tough_mudder_challenges', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    toughMudderSubChallengeId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    initialStepCount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    initialDistanceCount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    initialCaloriesCount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    isChallengeCompleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isSurveyCompleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    stepsInChallenge: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    distanceInChallenge: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    caloriesInChallenge: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  UserToughMudderChallenges.associate = (models) => {
    UserToughMudderChallenges.belongsTo(models.tough_mudder_sub_challenges, {
      foreignKey: 'toughMudderSubChallengeId',
      as: 'SubChallenge',
    });
    UserToughMudderChallenges.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
    });
  };

  return UserToughMudderChallenges;
};
