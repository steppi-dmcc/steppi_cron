const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const HealthMeasures = sequelize.define('health_measures', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    steps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    calories: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    distance: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    activeMinutes: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    fitnessDeviceId: DataTypes.BIGINT,
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  HealthMeasures.associate = (models) => {
    HealthMeasures.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
    HealthMeasures.belongsTo(models.fitness_devices, {
      foreignKey: 'fitnessDeviceId',
      as: 'device',
      onDelete: 'SET NULL',
    });
  };
  HealthMeasures.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return HealthMeasures;
};
