const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const RedeemedRewards = sequelize.define('redeemed_rewards', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    redemptionCode: {
      type: DataTypes.STRING(30),
      allowNull: false,
      unique: true,
    },
    redeemCode: {
      type: DataTypes.STRING(150),
      allowNull: true,
    },
    steppiAmount: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 2,
    },
    transactionId: {
      type: DataTypes.STRING(53),
      allowNull: false,
      defaultValue: sequelize.literal('get_unique_transaction_id()'),
    },
    redeemedSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    saved: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    rewardId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  RedeemedRewards.associate = (models) => {
    RedeemedRewards.belongsTo(models.rewards, {
      foreignKey: 'rewardId',
      as: 'reward',
      onDelete: 'CASCADE',
    });
    RedeemedRewards.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
  };
  RedeemedRewards.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return RedeemedRewards;
};
