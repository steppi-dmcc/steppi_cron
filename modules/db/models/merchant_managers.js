const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const MerchantManagers = sequelize.define('merchant_managers', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    managerId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    merchantId: {
      type: DataTypes.UUID,
      allowNull: false,
      unique: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  MerchantManagers.associate = (models) => {
    MerchantManagers.belongsTo(models.merchants, {
      foreignKey: 'merchantId',
      as: 'merchant',
      onDelete: 'CASCADE',
    });
    MerchantManagers.belongsTo(models.merchant_admins, {
      foreignKey: 'managerId',
      as: 'manager',
      onDelete: 'CASCADE',
    });
  };
  MerchantManagers.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return MerchantManagers;
};
