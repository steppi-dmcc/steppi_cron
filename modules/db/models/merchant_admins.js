const omit = require('lodash/omit');
const { hashSync, genSaltSync } = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const MerchantAdmins = sequelize.define('merchant_admins', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    merchantId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataTypes.STRING(350),
      allowNull: false,
    },
    image: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    role: {
      type: DataTypes.STRING(250),
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  MerchantAdmins.associate = (models) => {
    MerchantAdmins.belongsTo(models.merchants, {
      foreignKey: 'merchantId',
      as: 'merchant',
      onDelete: 'CASCADE',
    });
    MerchantAdmins.hasMany(models.merchant_managers, {
      foreignKey: 'managerId',
      as: 'branchMerchants',
      onDelete: 'CASCADE',
    });
  };
  MerchantAdmins.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  MerchantAdmins.prototype.securePassword = function securePassword() {
    return hashSync(this.dataValues.password, genSaltSync(10));
  };
  MerchantAdmins.prototype.safeModel = function safeModel() {
    return omit(this.toJSON(), ['password', 'createdAt', 'updatedAt']);
  };
  return MerchantAdmins;
};
