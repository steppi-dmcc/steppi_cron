const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Sessions = sequelize.define('sessions', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    accessToken: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    refreshToken: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    deviceId: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: true,
    },
    deviceToken: DataTypes.STRING(750),
    deviceType: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    twilioId: DataTypes.STRING(100),
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  Sessions.associate = (models) => {
    Sessions.belongsTo(models.users, {
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      as: 'user',
    });
  };
  Sessions.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return Sessions;
};
