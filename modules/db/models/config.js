module.exports = (sequelize, DataTypes) => {
  const Config = sequelize.define('config', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    value: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    textValue: DataTypes.TEXT,
    jsonValue: DataTypes.JSON,
  }, {
    tableName: 'config',
    timestamps: false
  });
  Config.associate = (models) => {
  };
  return Config;
};
