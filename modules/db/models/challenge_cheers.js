const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ChallengeCheers = sequelize.define('challenge_cheers', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    cheeredTo: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    cheeredBy: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ChallengeCheers.associate = (models) => {
    ChallengeCheers.belongsTo(models.challenge_users, {
      foreignKey: 'cheeredTo',
      as: 'cheered',
      onDelete: 'CASCADE',
    });
    ChallengeCheers.belongsTo(models.challenge_users, {
      foreignKey: 'cheeredBy',
      as: 'cheerer',
      onDelete: 'CASCADE',
    });
  };
  ChallengeCheers.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return ChallengeCheers;
};
