const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const FitnessDevices = sequelize.define('fitness_devices', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    deviceSupportType: {
      type: DataTypes.ARRAY(DataTypes.STRING(20)),
      allowNull: false,
    },
    deviceCode: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  FitnessDevices.associate = (models) => {
    FitnessDevices.hasMany(models.user_fitness_devices, {
      foreignKey: 'deviceId',
      as: 'users',
    });
    FitnessDevices.hasMany(models.users, {
      foreignKey: 'deviceId',
      as: 'deviceUsers',
    });
    FitnessDevices.hasMany(models.health_measures, {
      foreignKey: 'fitnessDeviceId',
      as: 'stepsOfUsers',
    });
  };
  FitnessDevices.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return FitnessDevices;
};
