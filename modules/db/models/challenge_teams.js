const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ChallengeTeams = sequelize.define('challenge_teams', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    corporateId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    challengeId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    email: DataTypes.STRING(150),
    phone: DataTypes.STRING(16),
    description: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    joinCode: {
      type: DataTypes.STRING(10),
      allowNull: true,
    },
    teamSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    engagementRate: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0,
    },
    avgSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    status: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      defaultValue: 1,
    },
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  ChallengeTeams.associate = (models) => {
    ChallengeTeams.belongsTo(models.corporations, {
      foreignKey: 'corporateId',
      as: 'corporation',
      onDelete: 'CASCADE',
    });
    ChallengeTeams.belongsTo(models.challenges, {
      foreignKey: 'challengeId',
      as: 'challenge',
      onDelete: 'CASCADE',
    });
    ChallengeTeams.hasMany(models.challenge_users, {
      foreignKey: 'challengeTeamId',
      as: 'teamMembers',
    });
    ChallengeTeams.hasMany(models.challenge_team_steps, {
      foreignKey: 'challengeTeamId',
      as: 'teamCumulativeSteps',
    });
  };
  ChallengeTeams.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['created_at', 'updated_at']);
  };
  return ChallengeTeams;
};
