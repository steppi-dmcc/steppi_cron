const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const VersionOnboardingScreens = sequelize.define('version_onboarding_screens', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    iosVersion: {
      type: DataTypes.BIGINT,
      allowNull: false,
      unique: true,
    },
    androidVersion: {
      type: DataTypes.BIGINT,
      allowNull: false,
      unique: true,
    },
    screens: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      allowNull: false,
      defaultValue: [],
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  VersionOnboardingScreens.associate = () => {};
  VersionOnboardingScreens.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return VersionOnboardingScreens;
};
