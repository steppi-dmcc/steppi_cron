const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Merchants = sequelize.define('merchants', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    logo: DataTypes.TEXT,
    images: DataTypes.ARRAY(DataTypes.TEXT),
    categories: DataTypes.ARRAY(DataTypes.TEXT),
    description: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    featured: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    site: DataTypes.STRING(250),
    tags: {
      type: DataTypes.ARRAY(DataTypes.STRING(255)),
      defaultValue: [],
    },
    isDigital: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  Merchants.associate = (models) => {
    Merchants.hasMany(models.merchant_stores, {
      foreignKey: 'merchantId',
      as: 'stores',
    });
    Merchants.hasMany(models.rewards, {
      foreignKey: 'merchantId',
      as: 'rewards',
    });
    Merchants.hasMany(models.user_merchant_favorites, {
      foreignKey: 'merchantId',
      as: 'likedBy',
    });
    Merchants.hasOne(models.merchant_managers, {
      foreignKey: 'merchantId',
      as: 'branchAdmin',
    });
  };
  Merchants.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return Merchants;
};
