const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Categories = sequelize.define('categories', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    logo: DataTypes.TEXT,
    colorCode: {
      type: DataTypes.STRING(7),
      defaultValue: '#000000',
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  Categories.associate = (models) => {
  };
  Categories.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return Categories;
};
