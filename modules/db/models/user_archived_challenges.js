const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const UserArchivedChallenges = sequelize.define('user_archived_challenges', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    challengeId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  UserArchivedChallenges.associate = (models) => {
    UserArchivedChallenges.belongsTo(models.challenges, {
      foreignKey: 'challengeId',
      as: 'challenge',
      onDelete: 'SET NULL',
    });
    UserArchivedChallenges.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
  };
  UserArchivedChallenges.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return UserArchivedChallenges;
};
