const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const RewardStores = sequelize.define('store_rewards', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    storeId: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    rewardId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  RewardStores.associate = (models) => {
    RewardStores.belongsTo(models.stores, {
      foreignKey: 'storeId',
      as: 'store',
      onDelete: 'CASCADE',
    });
    RewardStores.belongsTo(models.rewards, {
      foreignKey: 'rewardId',
      as: 'reward',
      onDelete: 'CASCADE',
    });
  };
  RewardStores.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return RewardStores;
};
