const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Teams = sequelize.define('teams', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    description: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    corporateId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    status: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      defaultValue: 1,
    },
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  Teams.associate = (models) => {
    Teams.belongsTo(models.corporations, {
      foreignKey: 'corporateId',
      as: 'corporation',
      onDelete: 'CASCADE',
    });
    Teams.hasMany(models.team_members, {
      foreignKey: 'teamId',
      as: 'members',
    });
  };
  Teams.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['created_at', 'updated_at']);
  };
  return Teams;
};
