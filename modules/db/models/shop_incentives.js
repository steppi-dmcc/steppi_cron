const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ShopIncentives = sequelize.define('shop_incentives', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    title: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    description: DataTypes.JSON,
    cost: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    images: DataTypes.ARRAY(DataTypes.TEXT),
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ShopIncentives.associate = (models) => {
  };
  ShopIncentives.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return ShopIncentives;
};
