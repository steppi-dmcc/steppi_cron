const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const CorporateInvitedEmails = sequelize.define('corporate_invited_emails', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    corporateId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false,
    },
    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
    indexes: [
      { fields: ['userId', 'corporationId'], unique: true },
    ],
  });
  CorporateInvitedEmails.associate = (models) => {
    CorporateInvitedEmails.belongsTo(models.corporations, {
      foreignKey: 'corporateId',
      as: 'corporation',
      onDelete: 'CASCADE',
    });
  };
  CorporateInvitedEmails.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };

  return CorporateInvitedEmails;
};
