module.exports = (sequelize, DataTypes) => {
  const UserDeviceTokens = sequelize.define('user_device_tokens', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    fitnessDeviceId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    token: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    secretToken: {
      type: DataTypes.STRING(250),
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  UserDeviceTokens.associate = (models) => {
    UserDeviceTokens.belongsTo(models.user_fitness_devices, {
      foreignKey: 'fitnessDeviceId',
      as: 'fitnessDevice',
    });
  };

  return UserDeviceTokens;
};
