const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ChallengeSteps = sequelize.define('challenge_steps', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    steps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    calories: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    distance: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    activeMinutes: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    challengedUserId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ChallengeSteps.associate = (models) => {
    ChallengeSteps.belongsTo(models.challenge_users, {
      foreignKey: 'challengedUserId',
      as: 'challengedUser',
      onDelete: 'CASCADE',
    });
  };
  ChallengeSteps.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return ChallengeSteps;
};
