const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Tags = sequelize.define('tags', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    tag: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    noOfMerchant: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    createdBy: DataTypes.UUID,
    createdAt: {
      type: DataTypes.DATE(3),
      defaultValue: Date.now,
      allowNull: false,
    },
  }, {
    timestamps: false,
  });
  Tags.associate = (models) => {
    Tags.belongsTo(models.admins, {
      foreignKey: 'createdBy',
      as: 'creator',
      onDelete: 'SET NULL',
    });
  };
  Tags.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt']);
  };
  return Tags;
};
