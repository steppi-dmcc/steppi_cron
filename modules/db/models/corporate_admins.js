const omit = require('lodash/omit');
const { hashSync, genSaltSync } = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const CorporateAdmins = sequelize.define('corporate_admins', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    corporateId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataTypes.STRING(350),
      allowNull: false,
    },
    image: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    role: {
      type: DataTypes.STRING(250),
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  CorporateAdmins.associate = (models) => {
    CorporateAdmins.belongsTo(models.corporations, {
      foreignKey: 'corporateId',
      as: 'corporation',
      onDelete: 'CASCADE',
    });
  };
  CorporateAdmins.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['created_at', 'updated_at']);
  };
  CorporateAdmins.prototype.securePassword = function () {
    return hashSync(this.dataValues.password, genSaltSync(10));
  };
  CorporateAdmins.prototype.safeModel = function safeModel() {
    return omit(this.toJSON(), ['password', 'created_at', 'updated_at']);
  };
  return CorporateAdmins;
};
