const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const UserChallengeCosts = sequelize.define('user_challenge_costs', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    challengeId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    dummyChallengeId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    costSteps: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    winningSteps: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  UserChallengeCosts.associate = (models) => {
    UserChallengeCosts.belongsTo(models.challenges, {
      foreignKey: 'challengeId',
      as: 'challenge',
      onDelete: 'SET NULL',
    });
    UserChallengeCosts.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
  };
  UserChallengeCosts.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return UserChallengeCosts;
};
