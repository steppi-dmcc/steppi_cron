const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ChallengeUsers = sequelize.define('challenge_users', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    challengeId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    challengeTeamId: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    totalSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    averageDaily: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    achievedDailyTargets: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    leftOn: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ChallengeUsers.associate = (models) => {
    ChallengeUsers.belongsTo(models.challenges, {
      foreignKey: 'challengeId',
      as: 'challenge',
      onDelete: 'CASCADE',
    });
    ChallengeUsers.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
    ChallengeUsers.belongsTo(models.challenge_teams, {
      foreignKey: 'challengeTeamId',
      as: 'challengeTeam',
      onDelete: 'CASCADE',
    });
    ChallengeUsers.hasMany(models.challenge_steps, {
      foreignKey: 'challengedUserId',
      as: 'steps',
    });
    ChallengeUsers.hasMany(models.challenge_cheers, {
      foreignKey: 'cheeredBy',
      as: 'cheerers',
    });
    ChallengeUsers.hasMany(models.challenge_cheers, {
      foreignKey: 'cheeredTo',
      as: 'cheers',
    });
  };
  ChallengeUsers.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return ChallengeUsers;
};
