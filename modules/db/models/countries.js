const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Countries = sequelize.define('countries', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    shortCode: {
      type: DataTypes.STRING(5),
      allowNull: false,
    },
    phoneCode: {
      type: DataTypes.STRING(5),
      allowNull: false,
    },
    currency: {
      type: DataTypes.STRING(5),
      allowNull: false,
    },
    flag: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
    indexes: [{
      unique: true,
      fields: ['shortCode', 'phoneCode'],
    }],
  });
  Countries.associate = (models) => {
    Countries.hasMany(models.users, {
      foreignKey: 'countryId',
      as: 'countryUsers',
    });
    Countries.hasMany(models.stores, {
      foreignKey: 'countryId',
      as: 'countryStores',
    });
  };
  Countries.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return Countries;
};
