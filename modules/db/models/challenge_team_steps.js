const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ChallengeTeamSteps = sequelize.define('challenge_team_steps', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    steps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    calories: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    distance: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    activeMinutes: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    noParticipantSynced: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    completion: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0,
    },
    totalSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    challengeTeamId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ChallengeTeamSteps.associate = (models) => {
    ChallengeTeamSteps.belongsTo(models.challenge_teams, {
      foreignKey: 'challengeTeamId',
      as: 'team',
      onDelete: 'CASCADE',
    });
  };
  ChallengeTeamSteps.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['created_at', 'updated_at']);
  };
  return ChallengeTeamSteps;
};
