module.exports = (sequelize, DataTypes) => {
  const ToughMudderChallenges = sequelize.define('tough_mudder_challenges', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    description: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    logo: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    limitedParticipation: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    allowedParticipants: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    startDate: DataTypes.DATEONLY,
    announcementTime: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    endDate: DataTypes.DATEONLY,
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ToughMudderChallenges.associate = () => {
  };

  return ToughMudderChallenges;
};
