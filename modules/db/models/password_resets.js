const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const PasswordResets = sequelize.define('password_resets', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },

    email: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  }, {
    createdAt: 'created_at',
    timestamps: true,
  });
  PasswordResets.associate = (models) => {};
  PasswordResets.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['created_at']);
  };
  return PasswordResets;
};
