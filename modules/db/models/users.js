const omit = require('lodash/omit');
const { hashSync, genSaltSync } = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('users', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    phoneNumber: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: true,
    },
    gender: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
    },
    latitude: DataTypes.DOUBLE,
    longitude: DataTypes.DOUBLE,
    password: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    facebookId: DataTypes.STRING(100),
    instagramId: DataTypes.STRING(100),
    appleId: DataTypes.STRING(250),
    dob: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    picture: DataTypes.STRING(550),
    corporateEmail: DataTypes.STRING(150),
    countryId: DataTypes.BIGINT,
    activeLevel: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    referredBy: DataTypes.UUID,
    usedReferral: DataTypes.STRING(8),
    deviceId: DataTypes.BIGINT,
    totalSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    lifetimeSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    totalSavings: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    lastSyncStamp: {
      type: DataTypes.DATE(3),
      allowNull: false,
      defaultValue: Date.now,
    },
    dailyGoal: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 10000,
    },
    lastUsedVersion: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    fingerprint: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  Users.associate = (models) => {
    Users.belongsTo(models.countries, {
      onDelete: 'SET NULL',
      foreignKey: 'countryId',
      as: 'country',
    });
    Users.hasOne(models.corporate_users, {
      foreignKey: 'userId',
      as: 'corporateUser',
    });
    Users.hasOne(models.sessions, {
      foreignKey: 'userId',
      as: 'session',
    });
    Users.hasOne(models.referral_codes, {
      foreignKey: 'userId',
      as: 'referralCode',
    });
    Users.hasOne(models.team_members, {
      foreignKey: 'userId',
      as: 'team',
    });
    Users.belongsTo(models.referral_codes, {
      onDelete: 'SET NULL',
      foreignKey: 'referredBy',
      as: 'referredByUser',
    });
    Users.belongsTo(models.fitness_devices, {
      onDelete: 'SET NULL',
      foreignKey: 'deviceId',
      as: 'device',
    });
    Users.hasMany(models.user_fitness_devices, {
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      as: 'devices',
    });
    Users.hasMany(models.reset_password_requests, {
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      as: 'resetPasswordRequests',
    });
    Users.hasMany(models.challenge_users, {
      foreignKey: 'userId',
      as: 'challenges',
    });
    Users.hasMany(models.received_referral_steps, {
      foreignKey: 'userId',
      as: 'referredUser',
    });
    Users.hasOne(models.received_referral_steps, {
      foreignKey: 'referredTo',
      as: 'referredFrom',
    });
    Users.hasMany(models.redeemed_rewards, {
      foreignKey: 'userId',
      as: 'redeemedRewards',
    });
    Users.hasMany(models.challenges, {
      foreignKey: 'createdBy',
      as: 'privateChallenges',
    });
  };

  Users.prototype.safeModel = function safeModel() {
    return omit(this.toJSON(), ['password', 'isDeleted', 'isActive', 'createdAt', 'updatedAt']);
  };
  Users.prototype.securePassword = function securePassword() {
    return hashSync(this.dataValues.password, genSaltSync(10));
  };
  // Users.prototype.get = Users.findById;

  return Users;
};
