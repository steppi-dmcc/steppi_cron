const omit = require('lodash/omit');

const randomString = (length, ...ranges) => () => {
  var str = "";
  while(length--) {
    var ind = Math.floor(Math.random() * ranges.length);
    var min = ranges[ind][0].charCodeAt(0),
        max = ranges[ind][1].charCodeAt(0);
    var c = Math.floor(Math.random() * (max - min + 1)) + min;
    str += String.fromCharCode(c);
  }
  return str;
}

module.exports = (sequelize, DataTypes) => {
  const Stores = sequelize.define('stores', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    description: DataTypes.TEXT,
    storePin: {
      type: DataTypes.STRING(4),
      allowNull: false,
      defaultValue: randomString(4, ['0', '9']),
    },
    contactEmail: {
      type: DataTypes.STRING(150),
      allowNull: false,
    },
    phoneNumber: {
      type: DataTypes.STRING(16),
      allowNull: false,
    },
    countryId: DataTypes.BIGINT,
    site: DataTypes.STRING(150),
    latitude: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    longitude: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    location: {
      type: DataTypes.STRING(350),
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  Stores.associate = (models) => {
    Stores.hasMany(models.merchant_stores, {
      foreignKey: 'storeId',
      as: 'merchants',
    });
    Stores.belongsTo(models.countries, {
      foreignKey: 'countryId',
      as: 'country',
      onDelete: 'SET NULL',
    });
    Stores.hasMany(models.store_rewards, {
      foreignKey: 'storeId',
      as: 'rewards',
    });
  };
  Stores.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return Stores;
};
