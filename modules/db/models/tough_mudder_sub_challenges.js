module.exports = (sequelize, DataTypes) => {
  const ToughMudderSubChallenges = sequelize.define('tough_mudder_sub_challenges', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    toughMudderId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'tough_mudder_challenges',
        key: 'id',
      },
      onDelete: 'CASCADE',
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    description: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    announcementTime: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    logo: DataTypes.TEXT,
    images: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      defaultValue: []
    },
    targetSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    targetDistance: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    targetCalories: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    startDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    endDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    surveyNumber: {
      type: DataTypes.STRING(150),
      allowNull: true,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ToughMudderSubChallenges.associate = (models) => {
    ToughMudderSubChallenges.belongsTo(models.tough_mudder_challenges, {
      foreignKey: 'toughMudderId',
      as: 'ToughMudderChallenge',
    });
    ToughMudderSubChallenges.hasMany(models.user_tough_mudder_challenges, {
      foreignKey: 'toughMudderSubChallengeId',
      as: 'participants',
    });
  };

  return ToughMudderSubChallenges;
};
