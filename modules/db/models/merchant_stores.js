const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const MerchantStores = sequelize.define('merchant_stores', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    merchantId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    storeId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  MerchantStores.associate = (models) => {
    MerchantStores.belongsTo(models.merchants, {
      foreignKey: 'merchantId',
      as: 'merchant',
      onDelete: 'CASCADE',
    });
    MerchantStores.belongsTo(models.stores, {
      foreignKey: 'storeId',
      as: 'store',
      onDelete: 'CASCADE',
    });
  };
  MerchantStores.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return MerchantStores;
};
