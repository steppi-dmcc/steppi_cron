const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const AppVersions = sequelize.define('app_versions', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    currentVersion: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    leastSupportVersion: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    appType: {
      type: DataTypes.STRING(10),
      allowNull: false,
      unique: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  AppVersions.associate = (models) => {};
  AppVersions.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return AppVersions;
};
