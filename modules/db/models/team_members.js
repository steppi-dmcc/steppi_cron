const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const TeamMembers = sequelize.define('team_members', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    teamId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    timestamps: true,
  });
  TeamMembers.associate = (models) => {
    TeamMembers.belongsTo(models.teams, {
      foreignKey: 'teamId',
      as: 'team',
      onDelete: 'CASCADE',
    });
    TeamMembers.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
  };
  TeamMembers.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['created_at', 'updated_at']);
  };
  return TeamMembers;
};
