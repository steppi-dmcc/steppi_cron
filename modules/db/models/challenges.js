const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Challenges = sequelize.define('challenges', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    description: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    logo: DataTypes.TEXT,
    images: DataTypes.ARRAY(DataTypes.TEXT),
    targetSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    targetCalories: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    targetMinutes: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    targetDistance: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    totalSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    totalCalories: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    totalMinutes: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    totalDistance: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    voucherDescription: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    startDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    showParticipants: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    limitedParticipation: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    allowedParticipants: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    averageDailySteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    perUserAverageDailySteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    participants: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    challengeType: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: 'STEPPI',
      enum: ['STEPPI', 'CORPORATE'],
    },
    type: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: 'STEPPI_CHALLENGE_1',
      enum: ['STEPPI_CHALLENGE_1', 'STEPPI_CHALLENGE_2', 'STEPPI_CHALLENGE_3'],
    },
    theme: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: 'NONE',
      enum: ['NONE', 'THEME1', 'THEME2'],
    },
    corporateId: DataTypes.UUID,
    corporateChallengeType: {
      type: DataTypes.STRING(80),
      allowNull: false,
      defaultValue: 'step',
    },
    endType: {
      type: DataTypes.STRING(25),
      allowNull: false,
      defaultValue: 'STEP',
      enum: ['STEP', 'END_DATE', 'STEP_END_DATE'],
    },
    corporateChallengeCap: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    endDate: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    announcementDate: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    announcementTime: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      defaultValue: 16,
    },
    isAnnounced: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isCompletionNotified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    joinCode: DataTypes.STRING(10),
    createdBy: DataTypes.UUID,
    createdFrom: DataTypes.UUID,
    costSteps: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    isPrivate: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    isArchived: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  Challenges.associate = (models) => {
    Challenges.belongsTo(models.corporations, {
      foreignKey: 'corporateId',
      as: 'corporation',
      onDelete: 'CASCADE',
    });
    Challenges.belongsTo(models.users, {
      foreignKey: 'createdBy',
      as: 'admin',
      onDelete: 'SET NULL',
    });
    Challenges.belongsTo(models.private_challenge_templates, {
      foreignKey: 'createdFrom',
      as: 'template',
      onDelete: 'SET NULL',
    });
    Challenges.hasMany(models.challenge_users, {
      foreignKey: 'challengeId',
      as: 'users',
    });
    Challenges.hasMany(models.challenge_teams, {
      foreignKey: 'challengeId',
      as: 'teams',
    });
  };
  Challenges.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return Challenges;
};
