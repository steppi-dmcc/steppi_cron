const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ReceivedReferralSteps = sequelize.define('received_referral_steps', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    steps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 1000,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    referredTo: DataTypes.UUID,
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ReceivedReferralSteps.associate = (models) => {
    ReceivedReferralSteps.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
      onDelete: 'CASCADE',
    });
    ReceivedReferralSteps.belongsTo(models.users, {
      foreignKey: 'referredTo',
      as: 'referredUser',
      onDelete: 'SET NULL',
    });
  };
  ReceivedReferralSteps.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return ReceivedReferralSteps;
};
