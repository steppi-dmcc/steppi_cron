const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const ResetPasswordRequests = sequelize.define('reset_password_requests', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id',
      },
      onDelete: 'CASCADE',
    },
    resetToken: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4,
    },
    isUsed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  ResetPasswordRequests.associate = (models) => {
    ResetPasswordRequests.belongsTo(models.users, {
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      as: 'user',
    });
  };
  ResetPasswordRequests.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return ResetPasswordRequests;
};
