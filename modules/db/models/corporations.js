const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Corporations = sequelize.define('corporations', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false,
    },
    location: {
      type: DataTypes.STRING(750),
      allowNull: false,
    },
    logo: {
      type: DataTypes.STRING(500),
      defaultValue: true,
      allowNull: false,
    },
    cover: {
      type: DataTypes.STRING(500),
      allowNull: true,
    },
    domains: {
      type: DataTypes.ARRAY(DataTypes.STRING(150)),
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING(500),
      allowNull: true,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  Corporations.associate = (models) => {
    Corporations.hasMany(models.corporate_users, {
      foreignKey: 'corporationId',
      as: 'corporateUsers',
    });
    Corporations.hasMany(models.challenges, {
      foreignKey: 'corporateId',
      as: 'challenges',
    });
    Corporations.hasMany(models.challenge_teams, {
      foreignKey: 'corporateId',
      as: 'challengeTeams',
    });
    Corporations.hasMany(models.corporate_admins, {
      foreignKey: 'corporateId',
      as: 'admins',
    });
  };
  Corporations.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };

  return Corporations;
};
