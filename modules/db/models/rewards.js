const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const Rewards = sequelize.define('rewards', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    description: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    steppiAmount: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 2,
    },
    isSteppiAmountPercentage: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    requiredSteps: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 1000,
    },
    estimateSaving: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    allowedRedemption: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    redeemed: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
    },
    rewardType: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      defaultValue: 1,
    },
    usagePerUser: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
    },
    merchantId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    noLimit: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false,
    },
    inAll: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    isFeatured: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    isMultiCoded: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    expireOn: {
      type: DataTypes.DATE(3),
      allowNull: false,
      defaultValue: Date.now,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  Rewards.associate = (models) => {
    Rewards.belongsTo(models.merchants, {
      foreignKey: 'merchantId',
      as: 'merchant',
      onDelete: 'CASCADE',
    });
    Rewards.hasMany(models.store_rewards, {
      foreignKey: 'rewardId',
      as: 'stores',
    });
    Rewards.hasMany(models.redeemed_rewards, {
      foreignKey: 'rewardId',
      as: 'redeemedBy',
    });
    Rewards.hasMany(models.reward_redeem_codes, {
      foreignKey: 'rewardId',
      as: 'redeemCodes',
    });
  };
  Rewards.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return Rewards;
};
