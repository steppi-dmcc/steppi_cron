const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const OtpDetails = sequelize.define('otp_details', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    otp: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    to: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
    for: {
      type: DataTypes.STRING(30),
      allowNull: false,
    },
    token: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
    },
    isAcquired: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    expiredAt: {
      type: DataTypes.DATE(3),
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    timestamps: true,
  });
  OtpDetails.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt']);
  };
  return OtpDetails;
};
