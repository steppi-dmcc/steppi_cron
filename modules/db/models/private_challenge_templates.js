const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const PrivateChallengesTemplates = sequelize.define('private_challenge_templates', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    description: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    logo: DataTypes.TEXT,
    images: DataTypes.ARRAY(DataTypes.TEXT),
    costSteps: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    noOf: {
      type: DataTypes.SMALLINT,
      allowNull: false,
    },
    type: {
      type: DataTypes.ENUM('day', 'week', 'month', 'year'),
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  PrivateChallengesTemplates.associate = (models) => {
    PrivateChallengesTemplates.hasMany(models.challenges, {
      foreignKey: 'createdFrom',
      as: 'challenges',
    });
  };
  PrivateChallengesTemplates.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return PrivateChallengesTemplates;
};
