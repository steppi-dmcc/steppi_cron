const omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
  const UserFitnessDevices = sequelize.define('user_fitness_devices', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    deviceId: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    accountId: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false,
    },
  }, {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    timestamps: true,
  });
  UserFitnessDevices.associate = (models) => {
    UserFitnessDevices.belongsTo(models.fitness_devices, {
      foreignKey: 'deviceId',
      as: 'device',
    });
    UserFitnessDevices.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'user',
    });
  };
  UserFitnessDevices.prototype.plainModel = function plainModel() {
    return omit(this.toJSON(), ['createdAt', 'updatedAt']);
  };
  return UserFitnessDevices;
};
