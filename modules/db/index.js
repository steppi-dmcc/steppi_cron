module.exports = (options, imports, register) => {
  const fs = require('fs');
  const path = require('path');
  const Sequelize = require('sequelize');

  const env = options.environment || 'development';
  const config = require('./config/database')[env];

  const sequelize = new Sequelize(config.database, config.username, config.password, config);

  sequelize
    .authenticate()
    .then(() => {
      console.log('connected to db');
    })
    .catch(err => console.error('Unable to connect to the database:', err));

  const db = {};
  const modelPath = `${__dirname}/models`;

  fs
    .readdirSync(modelPath)
    .filter(file => ((file.indexOf('.') !== 0) && (file !== 'index.js')))
    .forEach((file) => {
      const model = sequelize.import(path.join(modelPath, file));
      db[model.name] = model;
    });

  Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
      db[modelName].associate(db);
    }
  });

  db.sequelize = sequelize;
  db.Sequelize = Sequelize;

  register(null, {
    models: db,
  });
};
