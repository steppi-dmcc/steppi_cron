function ModuleConfig() {
  this.imports = {};
  this.options = {};
}

ModuleConfig.prototype.setImports = (imports) => {
  this.imports = imports;
};

ModuleConfig.prototype.setOptions = (options) => {
  this.options = options;
};

ModuleConfig.prototype.getImports = () => this.imports;

ModuleConfig.prototype.getOptions = () => this.options;

module.exports = new ModuleConfig();
