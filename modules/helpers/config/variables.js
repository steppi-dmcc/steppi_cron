const variable = {
  TRAVEL_MODE_DRIVING: 'driving',

  TRAFFIC_MODEL_BEST_GUESS: 'best_guess',
  TRAFFIC_MODEL_OPTIMISTIC: 'optimistic',
  TRAFFIC_MODEL_PESSIMISTIC: 'pessimistic',
};

module.exports = variable;
