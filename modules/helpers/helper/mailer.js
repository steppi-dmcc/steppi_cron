module.exports = () => {
  const {createTestAccount, createTransport} = require('nodemailer');
  const {readFileSync} = require('fs');
  const {join} = require('path');

  const moduleConfig = require('../config/module');
  const options = moduleConfig.getOptions();


  const config = require('../config/mailer/mailer')[options.env || 'development'];
  let transporter;

  if (config.demomail) {
    createTestAccount((err, acc) => {
      if (err) {
        return console.log('Unable to create SMTP test account', err);
      }
      transporter = createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: acc.user, // generated ethereal user
          pass: acc.pass // generated ethereal password
        },
      });
    });

  } else {
    transporter = createTransport(config.smtpConfig);
  }

  const mailIt = ({ to, subject, body: html }) => new Promise((res, rej) => {
    const defaultOptions = {
      from: config.from, // sender address
      bcc: config.bcc,
      to, // list of receivers
      subject, // Subject line
      html, // html body
    };
    if (transporter) {
      transporter.sendMail(defaultOptions, (error, info) => {
        if (error) {
          console.log('Error sening email', error);
          return rej(error);
        }
        res(info);
      });
    }
  });

  const convertHtmlToString = (template, object) => {
    const templateBody = readFileSync(join(__dirname, `/../views/${template}.html`));
    let htmlTemplate = templateBody.toString('utf8');

    for (const rv in object) { // eslint-disable-line
      htmlTemplate = htmlTemplate.replace(new RegExp(`{{${rv}}}`, 'g'), object[rv]);
    }

    return htmlTemplate;
  };
  return {
    mailIt,
    convertHtmlToString,
  };


};
