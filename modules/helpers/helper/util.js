module.exports = () => {
  const random = require('lodash/random');
  const { hashSync } = require('bcryptjs');
  const request = require('request');
  const moduleConfig = require('../config/module');
  const options = moduleConfig.getOptions();
  const imports = moduleConfig.getImports();

  const { sequelize } = imports.models;

  const isJSON = (string) => {
    try {
      const val = JSON.parse(string);
      return !!val;
    } catch (err) {
      return false;
    }
  };

  const randomPassword = () => {
    const lstr = 'abcdefghijklmnopqrstuvwxyz';
    const ustr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const nstr = '01234567890';
    const sstr = '@#$%';
    let ll = random(1, 7, false);
    let ul = random(1, 8 - ll, false);
    let nl = random(1, 9 - ul - ll, false);
    let sl = 10 - ul - nl - ll;
    let pass = '';
    while (pass.length !== 10) {
      const index = random(0, 4, false);
      if (index === 0 && ll > 0) {
        ll -= 1;
        pass += lstr.charAt(random(0, lstr.length - 1, false));
      }
      if (index === 1 && ul > 0) {
        ul -= 1;
        pass += ustr.charAt(random(0, ustr.length - 1, false));
      }
      if (index === 2 && nl > 0) {
        nl -= 1;
        pass += nstr.charAt(random(0, nstr.length - 1, false));
      }
      if (index === 3 && sl > 0) {
        sl -= 1;
        pass += sstr.charAt(random(0, sstr.length - 1, false));
      }
    }
    return pass;
  };

  const randomCode = () => {
    const lstr = 'abcdefghijklmnopqrstuvwxyz';
    const ustr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const nstr = '1234567890';
    let ll = random(1, 3, false);
    let ul = random(1, 4 - ll, false);
    let nl = 6 - ul - ll;
    let code = '';
    while (code.length !== 6) {
      const index = random(0, 4, false);
      if (index === 0 && ll > 0) {
        ll -= 1;
        code += lstr.charAt(random(0, lstr.length - 1, false));
      }
      if (index === 1 && ul > 0) {
        ul -= 1;
        code += ustr.charAt(random(0, ustr.length - 1, false));
      }
      if (index === 2 && nl > 0) {
        nl -= 1;
        code += nstr.charAt(random(0, nstr.length - 1, false));
      }
    }
    return code;
  };

  const randomOtp = () => Math.round((Math.random() * 8999) + 1000);

  const encrypt = (data, round = 10) => hashSync(data, round);

  const simplifyPhoneNumber = (str = '') => `+${str.replace(/\D/g, '')}`;
  const dbFunctionCall = async (name, params) => {
    try {
      const newParams = params.map((param) => {
        let p = param;
        if (!p && typeof p === 'undefined') {
          p = null;
        }
        if (typeof p === 'object' && Array.isArray(p)) {
          p = JSON.stringify(p);
        }
        return p;
      });

      let query = `select ${name} (`;
      Array.from({ length: newParams.length }).forEach((v, i) => {
        query += `$${i + 1}`;
        if (newParams.length !== i + 1) {
          query += ',';
        }
      });
      query += ')';

      const result = await sequelize.query(query, {
        bind: newParams,
      });
      const [[{ [name]: data }]] = result;
      return data;
    } catch (e) {
      throw e;
    }
  };

  const addParamsToText = (text, data) => {
    let body = text;
    for (const rv in data) { // eslint-disable-line
      body = body.replace(new RegExp(`{{${rv}}}`, 'g'), data[rv]);
    }
    return body;
  };

  const getQueryParams = (data = {}) => {
    let query = '';
    const dataArray = Object.entries(data);
    dataArray.map((keyVal, i) => {
      query += `${keyVal[0]}=${keyVal[1]}`;
      if (i < dataArray.length - 1) {
        query += '&';
      }
    });
    return query;
  };

  const createFireBaseDynamicLink = (data, optionalUrl) => new Promise((res) => {
    const link = `${options.appLinkUrl}${optionalUrl || '/'}?${getQueryParams(data)}`;
    request.post({
      json: {
        dynamicLinkInfo: {
          domainUriPrefix: options.dynamicLinkURL,
          link,
          androidInfo: {
            androidPackageName: options.androidAppBundle,
          },
          iosInfo: {
            iosBundleId: options.iosAppBundle,
            iosAppStoreId: options.iosStoreId,
          },
        },
      },
      url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${options.firebaseApiKey}`,
    }, (error, response, body) => {
      if (error || body.error) {
        return res(addParamsToText(options.spareDynamicLink, {
          link,
          ...options,
        }));
      }
      res(body.shortLink);
    });
  });

  const numberWithCommas = (x = '') => {
    const parts = x.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
  };

  return {
    isJSON,
    randomPassword,
    randomCode,
    randomOtp,
    encrypt,
    simplifyPhoneNumber,
    dbFunctionCall,
    createFireBaseDynamicLink,
    getQueryParams,
    numberWithCommas,
  };
};
