module.exports = () => {
  const moduleConfig = require('../config/module');
  const options = moduleConfig.getOptions();

  const twilio = require('twilio');
  const config = require('../config/notifier/notifier')[options.env || 'development'];

  const client = twilio(config.accountSid, config.authToken);
  const pushNotificationService = client.notify.services(config.notifySid);

  const getNotificationText = (text, data) => {
    let body = text;
    for (const rv in data) { // eslint-disable-line
      body = body.replace(new RegExp(`{{${rv}}}`, 'g'), data[rv]);
    }
    return body;
  };

  const sendPush = (deviceToken, data) => new Promise((res, rej) => {
    const obj = {
      identity: deviceToken,
      body: data.text,
      title: data.title,
      apn: data.extra,
      fcm: data.extra,
      data: data.extra,
    };
    pushNotificationService.notifications
      .create(obj)
      .then(res)
      .catch(rej)
      .done();
  });

  return {
    sendPush,
    getNotificationText,
  };
};
