module.exports = () => {
  const { CronJob } = require('cron');
  const { Op } = require('sequelize');
  const moment = require('moment-timezone');
  const groupBy = require('lodash/groupBy');

  const moduleConfig = require('../config/module');
  const imports = moduleConfig.getImports();
  const options = moduleConfig.getOptions();
  const constants = require('./constants')();
  const notifier = require('./notifier')();
  const mailer = require('./mailer')();
  const util = require('./util')();

  const { models = {} } = imports;
  const {
    challenge_steps: ChallengeSteps,
    challenge_teams: ChallengeTeams,
    challenge_users: ChallengeUsers,
    challenges: Challenges,
    corporations: Corporations,
    merchants: Merchants,
    received_referral_steps: ReceivedReferralSteps,
    rewards: Rewards,
    sessions: Sessions,
    team_discussions: TeamDiscussions,
    user_challenge_costs: UserChallengeCosts,
    users: Users,
    user_notifications: UserNotifications,
    sequelize,
  } = models;
  const crons = {};

  const updateUserChallengeCostLatest = async (winningSteps, where) => {
    try {
      const cost = await UserChallengeCosts.findOne({
        where,
        order: [['createdAt', 'desc']],
      });
      cost.winningSteps = winningSteps;
      await cost.save();
      return true;
    } catch (e) {
      throw e;
    }
  };

  const createNotificationAndSendToAll = async (
    receivers, body, mail = false, mailData = {}, extraParam = {}
  ) => {
    try {
      const data = body;
      const extra = {};
      const users = await Users.findAll({
        where: {
          id: { [Op.in]: receivers },
        },
        attributes: [
          'id',
          [sequelize.literal('"session"."deviceToken"'), 'deviceToken'],
          [sequelize.literal('"session"."twilioId"'), 'twilioId'],
          'corporateEmail',
          'email',
        ],
        include: [{
          model: Sessions,
          as: 'session',
          attributes: [],
        }],
        raw: true,
      });
      const extraData = {
        type: data.type,
      };
      extra.user = await Users.findOne({
        where: { id: data.relatedUserId },
        attributes: [
          'id',
          ['name', 'username2'],
          [sequelize.literal('"referredFrom"."steps"'), 'steps'],
        ],
        include: [{
          model: ReceivedReferralSteps,
          as: 'referredFrom',
          attributes: [],
        }],
        raw: true,
      });
      if (extra.user) {
        extraData.userId = extra.user.id;
      }
      extra.challenge = await Challenges.findOne({
        where: { id: data.relatedChallengeId },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          [sequelize.literal('"corporation"."name"'), 'corporateName'],
          'targetSteps',
          'startDate',
          'challengeType',
          'type',
          'theme',
          'isPrivate',
          'joinCode',
          [sequelize.literal(`(
            case when "challenges"."targetSteps" > 0 then
              round(("challenges"."totalSteps" * 100) / "challenges"."targetSteps")
            else
              0
            end
          )`), 'percentage'],
          [sequelize.literal('"admin"."name"'), 'challengerName'],
        ],
        include: [{
          model: Corporations,
          as: 'corporation',
          attributes: [],
        }, {
          model: Users,
          as: 'admin',
          attribute: [],
        }],
        raw: true,
      });
      if (extra.challenge) {
        extraData.challengeId = extra.challenge.id;
        extraData.challengeType = extra.challenge.challengeType;
        extraData.type = extra.challenge.type;
        extraData.theme = extra.challenge.theme;
        extraData.isPrivate = extra.challenge.isPrivate;
        extraData.joinCode = extra.challenge.joinCode;
        extra.challenge.startTime = moment(new Date(extra.challenge.startDate))
          .startOf('day')
          .format('DD MMM YYYY hh:mm A');
        extra.challenge.challengeDate = moment(new Date(extra.challenge.startDate))
          .format('dddd Do MMMM YYYY');
        if (mail) {
          extra.challenge.appLink = await util.createFireBaseDynamicLink({
            challenge_id: extra.challenge.id,
            challengeType: extra.challenge.challengeType,
            type: extra.challenge.type,
            theme: extra.challenge.theme,
            isPrivate: extra.challenge.isPrivate,
            joinCode: extra.challenge.joinCode,
          }, mail.optionalUrl);
        }
      }
      extra.challengeTeam = await ChallengeTeams.findOne({
        where: { id: data.relatedTeamId },
        attributes: [
          'id',
          [sequelize.literal('"challenge_teams"."name"#>>\'{en}\''), 'teamName'],
          [sequelize.literal(`(
            select count("id") from "challenge_users" where "challengeTeamId"="challenge_teams"."id"
          )`), 'teamMembers'],
          ['teamSteps', 'achievedSteps'],
          ['avgSteps', 'averageDaily'],
        ],
        raw: true,
      });
      if (data.relatedTeamId) {
        extra.challengeTeam.achievedSteps = util
          .numberWithCommas(extra.challengeTeam.achievedSteps);
        extra.challengeTeam.averageDaily = util
          .numberWithCommas(extra.challengeTeam.averageDaily);
      }
      extra.message = await TeamDiscussions.findOne({
        where: { id: data.relatedMessageId },
        attributes: [
          'id',
          'message',
          'createdAt',
        ],
        raw: true,
      });
      if (extra.message) {
        extra.message.sentTime = moment(new Date(extra.message.createdAt)).format('DD MMM YYYY hh:mm A');
      }
      extra.reward = await Rewards.findOne({
        where: { id: data.relatedRewardId },
        attributes: [
          'id',
          [sequelize.literal('"rewards"."name"#>>\'{en}\''), 'rewardName'],
          [sequelize.literal('"merchant"."name"#>>\'{en}\''), 'merchantName'],
          'merchantId',
          'rewardType',
          ['expireOn', 'expireTime'],
        ],
        include: [{
          model: Merchants,
          as: 'merchant',
          attribute: [],
        }],
        raw: true,
      });
      if (extra.reward) {
        extra.reward.expireTime = moment(new Date(extra.reward.expireTime)).format('DD MMM YYYY hh:mm A');
        extraData.rewardId = extra.reward.id;
        extraData.rewardType = extra.reward.rewardType;
      }
      data.text = notifier.getNotificationText(data.text, extra.user || {});
      data.text = notifier.getNotificationText(data.text, extra.reward || {});
      data.text = notifier.getNotificationText(data.text, extra.challenge || {});
      data.text = notifier.getNotificationText(data.text, extra.challengeTeam || {});
      data.text = notifier.getNotificationText(data.text, extra.message || {});
      data.text = notifier.getNotificationText(data.text, extraParam);
      data.title = (data.title || constants.notificationTitles[data.type]) || data.type;
      data.title = notifier.getNotificationText(data.title, extra.challenge || {});
      const notifications = users.map(user => ({
        ...data,
        userId: user.id,
        isSent: !(user && user.twilioId),
      }));
      const pushReceivers = users
        .filter(user => !!(user && user.twilioId))
        .map(user => user.deviceToken);
      if (pushReceivers && pushReceivers.length) {
        Array.from({ length: Math.ceil(pushReceivers.length / 20) })
          .some(() => {
            notifier.sendPush(pushReceivers.splice(0, 20), {
              title: data.title,
              text: data.text,
              extra: extraData,
            });
            return !(pushReceivers.length);
          });
      }
      if (mail) {
        mailer.mailIt({
          to: users.map(u => u[mail.receiver]),
          body: mailer.convertHtmlToString(mail.template, {
            notification: data.text,
            serverUrl: options.serverUrl,
            iosAppUrl: options.iosAppUrl,
            androidAppUrl: options.androidAppUrl,
            ...(extra.user || {}),
            ...(extra.reward || {}),
            ...(extra.challenge || {}),
            ...(extra.challengeTeam || {}),
            ...(extra.message || {}),
            ...mailData,
          }),
          subject: notifier.getNotificationText(mail.subject, {
            ...(extra.user || {}),
            ...(extra.reward || {}),
            ...(extra.challenge || {}),
            ...(extra.challengeTeam || {}),
            ...(extra.message || {}),
          }),
        });
      }
      UserNotifications.bulkCreate(notifications, {
        returning: false,
      });
      return true;
    } catch (e) {
      // eslint-disable-next-line
      console.error('error while sending to all notification', e);
      return true;
    }
  };

  const createUserNotification = async (
    body, bulk = false, mail = false, index = 1, mailData = {}, extraParam = {}
  ) => {
    try {
      const data = body;
      const user = await Users.findOne({
        where: { id: data.userId },
        include: [{
          model: Sessions,
          as: 'session',
        }],
      });
      user.username1 = user.name;
      data.isSent = !(user.session && user.session.twilioId);
      const extra = {};
      const extraData = {
        type: data.type,
      };
      extra.receiver = await Users.findOne({
        where: { id: data.userId },
        attributes: [
          'id',
          ['name', 'username'],
        ],
        raw: true,
      });
      extra.user = await Users.findOne({
        where: { id: data.relatedUserId },
        attributes: [
          'id',
          ['name', 'username2'],
          [sequelize.literal('"referredFrom"."steps"'), 'steps'],
        ],
        include: [{
          model: ReceivedReferralSteps,
          as: 'referredFrom',
          attributes: [],
        }],
        raw: true,
      });
      if (extra.user) {
        extraData.userId = extra.user.id;
        if (extra.receiver.id === extra.user.id) {
          extra.user.username2 = 'you are';
        }
      }
      extra.challenge = await Challenges.findOne({
        where: { id: data.relatedChallengeId },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          [sequelize.literal('"corporation"."name"'), 'corporateName'],
          'targetSteps',
          'startDate',
          'challengeType',
          'type',
          'theme',
          'isPrivate',
          'joinCode',
          [sequelize.literal(`(
            case when "challenges"."targetSteps" > 0 then
              round(("challenges"."totalSteps" * 100) / "challenges"."targetSteps")
            else
              0
            end
          )`), 'percentage'],
          [sequelize.literal('"admin"."name"'), 'challengerName'],
        ],
        include: [{
          model: Corporations,
          as: 'corporation',
          attributes: [],
        }, {
          model: Users,
          as: 'admin',
          attributes: [],
        }],
        raw: true,
      });
      if (extra.challenge) {
        extraData.challengeId = extra.challenge.id;
        extraData.challengeType = extra.challenge.challengeType;
        extraData.type = extra.challenge.type;
        extraData.theme = extra.challenge.theme;
        extraData.isPrivate = extra.challenge.isPrivate;
        extraData.joinCode = extra.challenge.joinCode;
        extra.challenge.startTime = moment(new Date(extra.challenge.startDate))
          .startOf('day')
          .format('DD MMM YYYY hh:mm A');
        if (mail) {
          extra.challenge.appLink = await util.createFireBaseDynamicLink({
            challenge_id: extra.challenge.id,
            challengeType: extra.challenge.challengeType,
            type: extra.challenge.type,
            theme: extra.challenge.theme,
            isPrivate: extra.challenge.isPrivate,
            joinCode: extra.challenge.joinCode,
          }, mail.optionalUrl);
        }
      }
      if (data.relatedTeamId) {
        extra.challengeTeam = await ChallengeTeams.findOne({
          where: { id: data.relatedTeamId },
          attributes: [
            'id',
            [sequelize.literal('"challenge_teams"."name"#>>\'{en}\''), 'teamName'],
            [sequelize.literal(`(
              select count("id") from "challenge_users" where "challengeTeamId"="challenge_teams"."id"
            )`), 'teamMembers'],
            ['teamSteps', 'achievedSteps'],
            ['avgSteps', 'averageDaily'],
          ],
          group: ['"challenge_teams"."id"'],
          raw: true,
        });
        extra.challengeTeam.achievedSteps = util
          .numberWithCommas(extra.challengeTeam.achievedSteps);
        extra.challengeTeam.averageDaily = util
          .numberWithCommas(extra.challengeTeam.averageDaily);
      }
      extra.message = await TeamDiscussions.findOne({
        where: { id: data.relatedMessageId },
        attributes: [
          'id',
          'message',
          'createdAt',
        ],
        raw: true,
      });
      if (extra.message) {
        extra.message.sentTime = moment(new Date(extra.message.createdAt)).format('DD MMM YYYY hh:mm A');
      }
      extra.reward = await Rewards.findOne({
        where: { id: data.relatedRewardId },
        attributes: [
          'id',
          [sequelize.literal('"rewards"."name"#>>\'{en}\''), 'rewardName'],
          [sequelize.literal('"merchant"."name"#>>\'{en}\''), 'merchantName'],
          'merchantId',
          'rewardType',
          ['expireOn', 'expireTime'],
        ],
        include: [{
          model: Merchants,
          as: 'merchant',
          attribute: [],
        }],
        raw: true,
      });
      if (extra.reward) {
        extra.reward.expireTime = moment(new Date(extra.reward.expireTime)).format('DD MMM YYYY hh:mm A');
        extraData.rewardId = extra.reward.id;
        extraData.rewardType = extra.reward.rewardType;
      }
      data.text = notifier.getNotificationText(data.text, extra.receiver || {});
      data.text = notifier.getNotificationText(data.text, extra.user || {});
      data.text = notifier.getNotificationText(data.text, extra.reward || {});
      data.text = notifier.getNotificationText(data.text, extra.challenge || {});
      data.text = notifier.getNotificationText(data.text, extra.challengeTeam || {});
      data.text = notifier.getNotificationText(data.text, extra.message || {});
      data.text = notifier.getNotificationText(data.text, extraParam);
      data.title = (data.title || constants.notificationTitles[data.type]) || data.type;
      data.title = notifier.getNotificationText(data.title, extra.challenge || {});
      if (!data.isSent) {
        setTimeout(() => notifier.sendPush([user.session.deviceToken], {
          text: data.text,
          title: data.title,
          extra: extraData,
        }), index * 500);
      }
      if (mail) {
        mailer.mailIt({
          to: user[mail.receiver],
          body: mailer.convertHtmlToString(mail.template, {
            notification: data.text,
            serverUrl: options.serverUrl,
            iosAppUrl: options.iosAppUrl,
            androidAppUrl: options.androidAppUrl,
            teamName: 'No Team',
            achievedSteps: 0,
            teamMembers: 0,
            ...(extra.user || {}),
            ...(extra.reward || {}),
            ...(extra.challenge || {}),
            ...(extra.challengeTeam || {}),
            ...(extra.message || {}),
            ...mailData,
          }),
          subject: notifier.getNotificationText(mail.subject, {
            ...(extra.user || {}),
            ...(extra.reward || {}),
            ...(extra.challenge || {}),
            ...(extra.challengeTeam || {}),
            ...(extra.message || {}),
          }),
        });
      }
      if (bulk) return data;
      UserNotifications.create(data);
    } catch (e) {
      throw e;
    }
  };

  crons.syncNowNoteNotification = async () => {
    try {
      const users = await Users.findAll({
        attributes: [
          'id',
          ['name', 'username1'],
          'lastSyncStamp',
        ],
        where: {
          [Op.and]: [{
            lastSyncStamp: {
              [Op.gte]: moment()
                .startOf('day')
                .subtract(7, 'days')
                .toISOString(),
            },
          }, {
            isActive: true,
          }, {
            lastSyncStamp: {
              [Op.lte]: moment()
                .endOf('day')
                .subtract(7, 'days')
                .toISOString(),
            },
          }],
        },
        include: [{
          model: Sessions,
          as: 'session',
        }],
      });
      if (users.length) {
        const notifications = users.map(({ dataValues: user }) => {
          const text = notifier
            .getNotificationText(
              constants.notificationTexts.syncNowNote,
              user,
            );
          return {
            userId: user.id,
            type: constants.notificationTypes.syncNowNote,
            text,
            isSent: user.session ? !!user.session.twilioId : true,
            user,
          };
        });
        // await UserNotifications.bulkCreate(notifications.map(n => omit(n, ['user'])), {
        //   fields: ['userId', 'type', 'text', 'isSent'],
        // });
        const pushNotifications = notifications.filter(n => !n.isSent);
        if (pushNotifications.length) {
          await Promise.all(pushNotifications
            .map(({ user, text, type }) => notifier.sendPush([user.session.deviceToken], {
              text,
              title: constants.notificationTitles[type],
              extra: {},
            })));
        }
      }
    } catch (err) {
      // eslint-disable-next-line
      console.error('error at sync now notification cron', err);
    }
  };

  crons.corporateChallengeCompleteNotification = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          endDate: { [Op.lte]: moment().tz('Asia/Dubai')._d },
          isCompletionNotified: false,
          isDeleted: false,
          isActive: true,
          [Op.or]: [{
            challengeType: constants.challengeTypeLabel.corporate,
            type: constants.challengeSubTypeLabel.pc1,
          }, {
            challengeType: constants.challengeTypeLabel.steppi,
            type: constants.challengeSubTypeLabel.pc2,
            // }, {
            //   challengeType: constants.challengeTypeLabel.steppi,
            //   type: constants.challengeSubTypeLabel.pc4,
          }],
        },
        include: [{
          model: ChallengeTeams,
          as: 'teams',
          attributes: [
            'id',
            [sequelize.literal('"teams"."name"#>>\'{en}\''), 'name'],
            [sequelize.literal('"teams"."description"#>>\'{en}\''), 'description'],
            'status',
            [sequelize.literal(`(
              select sum("steps") from "challenge_team_steps"
              where "challengeTeamId"="teams"."id"
            )`), 'teamSteps'],
            ['avgSteps', 'averageDaily'],
          ],
          include: [{
            model: ChallengeUsers,
            as: 'teamMembers',
            where: { leftOn: null },
            include: [{
              model: Users,
              as: 'user',
              attributes: [
                'id',
                'name',
                'picture',
                [sequelize.literal(`(
                    case when sum("teams->teamMembers->steps"."steps") is null then
                      0
                    else
                      sum("teams->teamMembers->steps"."steps")
                    end
                  )`), 'steps'],
              ],
              required: true,
            }, {
              model: ChallengeSteps,
              as: 'steps',
              attributes: [],
            }],
            required: true,
          }],
        }, {
          model: Corporations,
          as: 'corporation',
          attributes: [
            'name',
            'logo',
          ],
        }],
        group: ['"challenges"."id"', '"corporation"."id"', '"teams"."id"', '"teams->teamMembers"."id"', '"teams->teamMembers->user"."id"'],
      });
      if (challenges.length) {
        let _promise = [];
        challenges.map((c) => {
          const challenge = c.toJSON();
          let winnerTeam = null;
          let teamSteps = 0;
          if (challenge.teams && challenge.teams.length) {
            challenge.teams.forEach((team) => {
              if (Number(team.teamSteps) > teamSteps) {
                teamSteps = Number(team.teamSteps);
                winnerTeam = team;
              }
            });
            if (winnerTeam && winnerTeam.id) {
              createNotificationAndSendToAll(
                winnerTeam.teamMembers.map(tm => tm.userId),
                {
                  relatedTeamId: winnerTeam.id,
                  relatedChallengeId: challenge.id,
                  type: challenge.challengeType === constants.challengeTypeLabel.corporate ?
                    constants.notificationTypes.corporateChallengeCompletedWinner :
                    constants.notificationTypes.pc2ChallengeCompletedWinner,
                  text: challenge.challengeType === constants.challengeTypeLabel.corporate ?
                    constants.notificationTexts.corporateChallengeCompletedWinner :
                    constants.notificationTexts.pc2ChallengeCompletedWinner,
                },
                challenge.challengeType === constants.challengeTypeLabel.corporate ? {
                  subject: constants.mailSubject.corporateChallengeCompleted,
                  receiver: 'corporateEmail',
                  template: 'corporate-challenge-end',
                  optionalUrl: `/api/v1/challenge/get-corporate-challenge-stats/corporate-challenge-end/${challenge.id}`,
                } : false,
                challenge.challengeType === constants.challengeTypeLabel.corporate ? {
                  corporateLogo: (challenge.corporation && challenge.corporation.logo) || '',
                  teamMembers: winnerTeam.teamMembers.length,
                } : undefined
              );
            }
            challenge.teams.filter(t => !(winnerTeam && t.id === winnerTeam.id))
              .forEach((team) => {
                let type = !winnerTeam ?
                  constants.notificationTypes.corporateChallengeNoWinner :
                  constants.notificationTypes.corporateChallengeCompleted;
                let text = !winnerTeam ?
                  constants.notificationTexts.corporateChallengeNoWinner :
                  constants.notificationTexts.corporateChallengeCompleted;

                if (challenge.challengeType === constants.challengeTypeLabel.steppi) {
                  type = !winnerTeam ?
                    constants.notificationTypes.pc2ChallengeNoWinner :
                    constants.notificationTypes.pc2ChallengeCompleted;
                  text = !winnerTeam ?
                    constants.notificationTexts.pc2ChallengeNoWinner :
                    constants.notificationTexts.pc2ChallengeCompleted;
                }
                _promise = _promise
                  .concat(team.teamMembers.map((member, i) => createUserNotification(
                    {
                      relatedTeamId: winnerTeam ? winnerTeam.id : null,
                      userId: member.userId,
                      relatedChallengeId: member.challengeId,
                      type,
                      text,
                    },
                    true,
                    challenge.challengeType === constants.challengeTypeLabel.corporate ? {
                      subject: constants.mailSubject.corporateChallengeCompleted,
                      receiver: 'corporateEmail',
                      template: 'corporate-challenge-end',
                      optionalUrl: `/api/v1/challenge/get-corporate-challenge-stats/corporate-challenge-end/${challenge.id}`,
                    } : undefined,
                    Math.floor(i / 20),
                    challenge.challengeType === constants.challengeTypeLabel.corporate ? {
                      corporateLogo: (challenge.corporation && challenge.corporation.logo) || '',
                      teamMembers: winnerTeam ? util.numberWithCommas(winnerTeam.teamMembers.length) : 'N/A',
                      averageDaily: winnerTeam ? util.numberWithCommas(winnerTeam.averageDaily) : 'N/A',
                      achievedSteps: winnerTeam ? util.numberWithCommas(winnerTeam.teamSteps) : 'N/A',
                    } : undefined
                  )));
              });
          }
        });
        let notifications = await Promise.all(_promise);
        notifications = notifications.filter(n => !!n);
        if (notifications.length) {
          await UserNotifications.bulkCreate(notifications, { returning: true });
        }
        Challenges.update({
          isCompletionNotified: true,
        }, {
          where: {
            id: { [Op.in]: challenges.map(({ dataValues }) => dataValues.id) },
          },
        });
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error in corporate challenge completion notification cron: ', e);
    }
  };

  crons.steppiChallengeCompleteNotification = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          endDate: moment().tz('Asia/Dubai').subtract(1, 'hour'),
          challengeType: constants.challengeTypeLabel.steppi,
          type: {
            [Op.notIn]: [
              constants.challengeSubTypeLabel.pc2
            ],
          },
          isCompletionNotified: false,
          isActive: true,
          isDeleted: false,
        },
        include: [{
          model: ChallengeUsers,
          as: 'users',
          where: { leftOn: null },
          attributes: ['id', 'userId', 'challengeId', 'createdAt', 'totalSteps', 'achievedDailyTargets'],
        }],
        order: [
          [sequelize.literal('"users"."totalSteps"'), 'desc'],
          [sequelize.literal('"users"."createdAt"'), 'asc'],
          [sequelize.literal('"users"."userId"'), 'asc'],
        ],
      });
      if (challenges.length) {
        let _promise = [];
        const winnerPromise = [];
        const updateUserPromise = [];
        challenges.map((c) => {
          const challenge = c.toJSON();
          let winnerUser = null;
          if (challenge.users && challenge.users.length) {
            if (challenge.isPrivate) {
              const groupedUsers = groupBy(challenge.users, 'totalSteps');
              const steps = Object.keys(groupedUsers).sort((a, b) => Number(b) - Number(a));
              let tiedUsers = [];
              let otherUsers = [];
              steps.forEach((step, i) => {
                if (i === 0) {
                  const randomIndex = 0;
                  winnerUser = groupedUsers[step][randomIndex];
                  tiedUsers = tiedUsers.concat(groupedUsers[step]);
                } else {
                  otherUsers = otherUsers.concat(groupedUsers[step]);
                }
              });
              winnerPromise.push(updateUserChallengeCostLatest(
                Number(challenge.participants) * Number(challenge.costSteps),
                { challengeId: challenge.id, userId: winnerUser.userId }
              ));
              updateUserPromise.push(Users.update({
                totalSteps: sequelize.literal('get_user_calculated_health_data("users"."id")'),
              }, {
                where: { id: winnerUser.userId },
              }));
              const otherSame = tiedUsers.filter(u => u.userId !== winnerUser.userId);
              if (otherSame.length) {
                createNotificationAndSendToAll(otherSame.map(u => u.userId), {
                  relatedChallengeId: challenge.id,
                  relatedUserId: winnerUser.userId,
                  type: constants.notificationTypes.privateChallengeEnd,
                  text: constants.notificationTexts.privateChallengeEndNoWinner,
                }, false, {});
              }
              _promise.push(createUserNotification({
                userId: winnerUser.userId,
                relatedUserId: tiedUsers.length ? winnerUser.userId : undefined,
                relatedChallengeId: winnerUser.challengeId,
                type: constants.notificationTypes.privateChallengeEnd,
                text: tiedUsers.length > 1 ?
                  constants.notificationTexts.privateChallengeEndNoWinner :
                  constants.notificationTexts.privateChallengeEndWinner,
              }, true, false, 1, {}, {
                winnerSteps: util.numberWithCommas(winnerUser.totalSteps),
              }));
              if (otherUsers.length) {
                createNotificationAndSendToAll(otherUsers.map(u => u.userId), {
                  relatedChallengeId: challenge.id,
                  relatedUserId: winnerUser.userId,
                  type: constants.notificationTypes.privateChallengeEnd,
                  text: constants.notificationTexts.privateChallengeEndParticipants,
                }, false, {}, {
                  winnerSteps: util.numberWithCommas(winnerUser.totalSteps),
                });
              }
            } else if (
              challenge.type === constants.challengeSubTypeLabel.pc3
            ) {
              const challengeStart = moment(new Date(challenge.startDate)).startOf('day');
              const challengeEnd = moment(new Date(challenge.endDate)).endOf('day');
              const challengeDays = Math.round(challengeEnd.diff(challengeStart, 'seconds') / 60 / 60 / 24);
              _promise = _promise
                .concat(challenge.users.map((user, i) => createUserNotification({
                  userId: user.userId,
                  relatedChallengeId: user.challengeId,
                  type: constants.notificationTypes.pc3ChallengeEnd,
                  text: constants.notificationTexts.pc3ChallengeEnd,
                }, true, false, Math.floor(i / 20), {}, {
                  challengeRatio: `${user.achievedDailyTargets}/${challengeDays}`,
                })));
            } else {
              _promise = _promise
                .concat(challenge.users.map((user, i) => createUserNotification({
                  userId: user.userId,
                  relatedChallengeId: user.challengeId,
                  type: constants.notificationTypes[
                    challenge.type === constants.challengeSubTypeLabel.pc4 ?
                      'dfcCompleted' :
                      'challengeTargetStepsAchieved'
                  ],
                  text: constants.notificationTexts[
                    challenge.type === constants.challengeSubTypeLabel.pc4 ?
                      'dfcCompleted' : (
                        challenge.endType === constants.challengeEndType.endDate ?
                          'steppiChallengeDateEnded' :
                          'steppiChallengeGoalNotAcheived'
                      )
                  ],
                }, true, false, Math.floor(i / 20))));
            }
          }
        });
        let notifications = await Promise.all(_promise);
        await Promise.all(winnerPromise);
        await Promise.all(updateUserPromise);
        notifications = notifications.filter(n => !!n);
        if (notifications.length) {
          await UserNotifications.bulkCreate(notifications, { returning: true });
        }
        Challenges.update({
          isCompletionNotified: true,
        }, {
          where: {
            id: { [Op.in]: challenges.map(({ dataValues }) => dataValues.id) },
          },
        });
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error in corporate challenge completion notification cron: ', e);
    }
  };

  crons.challengeAnnouncement = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          challengeType: constants.challengeTypeLabel.corporate,
          announcementDate: moment().tz('Asia/Dubai')._d,
          announcementTime: Number(moment().tz('Asia/Dubai').format('HH')),
          isActive: true,
          isAnnounced: false,
          isDeleted: false,
        },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          'corporateId',
          'startDate',
          'endDate',
          'type',
          'challengeType',
          [sequelize.literal('"challenges"."voucherDescription"#>>\'{en}\''), 'voucherDescription'],
        ],
        include: [{
          model: ChallengeUsers,
          as: 'users',
          where: { leftOn: null },
          attributes: [
            'id',
            [sequelize.literal('"users->user"."id"'), 'userId'],
          ],
          include: [{
            model: Users,
            as: 'user',
            required: true,
            attributes: [],
          }],
        }, {
          model: Corporations,
          as: 'corporation',
          attributes: [
            'name',
            'logo',
          ],
        }],
      });
      if (challenges.length) {
        const ids = [];
        await Promise.all(challenges.map((challengeModel) => {
          const challenge = challengeModel.toJSON();
          const mailData = {
            startDate: moment(new Date(challenge.startDate)).startOf('day').format('DD/MM/YYYY'),
            endDate: moment(new Date(challenge.endDate)).endOf('day').format('DD/MM/YYYY'),
            rewardDetails: challenge.voucherDescription || 'Not Available',
            participants: challenge.users.length,
            corporateLogo: (challenge.corporation && challenge.corporation.logo) || `${options.serverUrl}/images/logo2.png`,
          };
          ids.push(challenge.id);
          return createNotificationAndSendToAll(
            challenge.users.map(user => user.userId), {
            type: constants.notificationTypes.corporateChallengeAboutToStart,
            text: constants.notificationTexts.corporateChallengeAboutToStart,
            relatedChallengeId: challenge.id,
            relatedCorporateId: challenge.corporateId,
          }, {
            subject: constants.mailSubject.corporateChallengeAboutToStart,
            receiver: 'corporateEmail',
            template: 'corporate-challenge-announcement',
            optionalUrl: `/api/v1/challenge/get-corporate-challenge-stats/corporate-challenge-announcement/${challenge.id}`,
          },
            mailData
          );
        }));
        await Challenges.update({
          isAnnounced: true,
        }, {
          where: { id: { [Op.in]: ids } },
        });
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeAnnouncementCron: ', e);
    }
  };

  crons.pc2ChallengeAnnouncement = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          challengeType: constants.challengeTypeLabel.steppi,
          type: {
            [Op.in]: [
              constants.challengeSubTypeLabel.pc2,
              constants.challengeSubTypeLabel.pc4,
            ]
          },
          announcementDate: moment().tz('Asia/Dubai')._d,
          announcementTime: Number(moment().tz('Asia/Dubai').format('HH')),
          isActive: true,
          isAnnounced: false,
          isDeleted: false,
        },
        attributes: ['id', 'type'],
      });
      const users = await Users.findAll({
        where: { isActive: true, isDeleted: false },
        attributes: ['id'],
        raw: true,
      });
      if (challenges.length) {
        const ids = [];
        await Promise.all(challenges.map((challengeModel) => {
          const challenge = challengeModel.toJSON();
          ids.push(challenge.id);
          return createNotificationAndSendToAll(
            users.map(user => user.id),
            {
              type: constants.notificationTypes[
                challenge.type === constants.challengeSubTypeLabel.pc4 ?
                  'dfcAboutToStart' :
                  'pc2ChallengeAboutToStart'
              ],
              text: constants.notificationTexts[
                challenge.type === constants.challengeSubTypeLabel.pc4 ?
                  'dfcAboutToStart' :
                  'pc2ChallengeAboutToStart'
              ],
              relatedChallengeId: challenge.id,
            }
          );
        }));
        await Challenges.update({
          isAnnounced: true,
        }, {
          where: { id: { [Op.in]: ids } },
        });
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeAnnouncementCron: ', e);
    }
  };

  crons.corporateChallengeStartingSoonCron = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          startDate: moment().tz('Asia/Dubai').add(1, 'day')._d,
          isActive: true,
          isDeleted: false,
          [Op.or]: [{
            challengeType: constants.challengeTypeLabel.corporate,
            type: constants.challengeSubTypeLabel.pc1,
          }, {
            challengeType: constants.challengeTypeLabel.steppi,
            type: constants.challengeSubTypeLabel.pc2,
          }, {
            challengeType: constants.challengeTypeLabel.steppi,
            type: constants.challengeSubTypeLabel.pc4,
          }],
        },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          [sequelize.literal('"challenges"."voucherDescription"#>>\'{en}\''), 'voucherDescription'],
          'challengeType',
          'type',
          'startDate',
          'endDate',
          'corporateId',
        ],
        include: [{
          model: ChallengeTeams,
          as: 'teams',
          attributes: [
            'id',
            [sequelize.literal('"teams"."name"#>>\'{en}\''), 'name'],
            [sequelize.literal('"teams"."description"#>>\'{en}\''), 'description'],
            'status',
          ],
          include: [{
            model: ChallengeUsers,
            as: 'teamMembers',
            where: { leftOn: null },
            required: true,
          }],
        }, {
          model: Corporations,
          as: 'corporation',
          attributes: [
            'name',
            'logo',
          ],
        }],
      });
      if (challenges.length) {
        await Promise.all(challenges.map((c) => {
          const challenge = c.toJSON();
          return Promise.all(challenge.teams
            .map(team => createNotificationAndSendToAll(
              team.teamMembers.map(u => u.userId),
              {
                type: constants.notificationTypes[
                  challenge.challengeType === constants.challengeTypeLabel.corporate ?
                    'corporateChallengeStartingSoon' :
                    (challenge.type === constants.challengeSubTypeLabel.pc4 ?
                      'dfcStartingSoon' :
                      'pc2ChallengeStartingSoon'
                    )
                ],
                text: constants.notificationTexts[
                  challenge.challengeType === constants.challengeTypeLabel.corporate ?
                    'corporateChallengeStartingSoon' :
                    (challenge.type === constants.challengeSubTypeLabel.pc4 ?
                      'dfcStartingSoon' :
                      'pc2ChallengeStartingSoon'
                    )
                ],
                relatedChallengeId: challenge.id,
                relatedTeamId: team.id,
                relatedCorporateId: challenge.corporateId,
              },
              challenge.challengeType === constants.challengeTypeLabel.corporate ? {
                subject: constants.mailSubject.corporateChallengeStartingSoon,
                receiver: 'corporateEmail',
                template: 'corporate-challenge-start',
                optionalUrl: `/api/v1/challenge/get-corporate-challenge-stats/corporate-challenge-start/${challenge.id}`,
              } : undefined,
              challenge.challengeType === constants.challengeTypeLabel.corporate ? {
                startDate: moment(new Date(challenge.startDate)).startOf('day').format('DD/MM/YYYY'),
                startDateOnly: moment(new Date(challenge.startDate)).startOf('day').format('MMMM D'),
                endDate: moment(new Date(challenge.endDate)).endOf('day').format('DD/MM/YYYY'),
                rewardDetails: challenge.voucherDescription || 'Not Available',
                participants: (team.teamMembers && team.teamMembers.length) || 0,
                corporateLogo: (challenge.corporation && challenge.corporation.logo) || `${options.serverUrl}/images/logo2.png`,
              } : undefined
            )));
        }));
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeStartingCron: ', e);
    }
  };

  crons.publicChallengeStartingSoon = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          startDate: moment().tz('Asia/Dubai').add(1, 'day')._d,
          challengeType: constants.challengeTypeLabel.steppi,
          type: {
            [Op.notIn]: [
              constants.challengeSubTypeLabel.pc2,
              constants.challengeSubTypeLabel.pc4
            ]
          },
          isActive: true,
          isDeleted: false,
          isPrivate: false,
          createdBy: null,
          createdFrom: null,
        },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          'challengeType',
          'corporateId',
        ],
        include: [{
          model: ChallengeUsers,
          as: 'users',
          where: { leftOn: null },
          attributes: [
            'id',
            'userId',
          ],
          required: true,
        }],
      });
      if (challenges.length) {
        await Promise.all(challenges.map((c) => {
          const challenge = c.toJSON();
          return createNotificationAndSendToAll(challenge.users.map(u => u.userId), {
            type: constants.notificationTypes.steppiChallengeStartingSoon,
            text: constants.notificationTexts.steppiChallengeStartingSoon,
            relatedChallengeId: challenge.id,
          });
        }));
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeStartingCron: ', e);
    }
  };

  crons.privateChallengeStartingSoon = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          startDate: moment().tz('Asia/Dubai').add(1, 'day')._d,
          challengeType: constants.challengeTypeLabel.steppi,
          isActive: true,
          isDeleted: false,
          isPrivate: true,
        },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          'challengeType',
          'corporateId',
        ],
        include: [{
          model: ChallengeUsers,
          as: 'users',
          where: { leftOn: null },
          attributes: [
            'id',
            'userId',
          ],
          required: true,
        }],
      });
      if (challenges.length) {
        let _promise = [];
        challenges.map((c) => {
          const challenge = c.toJSON();
          if (challenge.users && challenge.users.length) {
            _promise = _promise
              .concat(challenge.users.map((user, i) => createUserNotification({
                userId: user.userId,
                relatedChallengeId: challenge.id,
                type: constants.notificationTypes.privateChallengeStartingSoon,
                text: constants.notificationTexts.privateChallengeStartingSoon,
              }, true, false, Math.floor(i / 20))));
          }
        });
        let notifications = await Promise.all(_promise);
        notifications = notifications.filter(n => !!n);
        if (notifications.length) {
          await UserNotifications.bulkCreate(notifications, { returning: true });
        }
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeStartingCron: ', e);
    }
  };

  crons.privateChallengeStarted = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          startDate: moment().tz('Asia/Dubai')._d,
          challengeType: constants.challengeTypeLabel.steppi,
          isActive: true,
          isDeleted: false,
          isPrivate: true,
        },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          'challengeType',
          'corporateId',
        ],
        include: [{
          model: ChallengeUsers,
          as: 'users',
          where: { leftOn: null },
          attributes: [
            'id',
            'userId',
          ],
          required: true,
        }],
      });
      if (challenges.length) {
        let _promise = [];
        challenges.map((c) => {
          const challenge = c.toJSON();
          if (challenge.users && challenge.users.length) {
            _promise = _promise
              .concat(challenge.users.map((user, i) => createUserNotification({
                userId: user.userId,
                type: constants.notificationTypes.privateChallengeStarted,
                text: constants.notificationTexts.privateChallengeStarted,
                relatedChallengeId: challenge.id,
              }, true, false, Math.floor(i / 20))));
          }
        });
        let notifications = await Promise.all(_promise);
        notifications = notifications.filter(n => !!n);
        if (notifications.length) {
          await UserNotifications.bulkCreate(notifications, { returning: true });
        }
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeStartingCron: ', e);
    }
  };

  crons.privateChallengeUpdates = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          startDate: { [Op.lt]: moment().tz('Asia/Dubai')._d },
          endDate: { [Op.gte]: moment().tz('Asia/Dubai')._d },
          challengeType: constants.challengeTypeLabel.steppi,
          isActive: true,
          isDeleted: false,
          isPrivate: true,
        },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          'challengeType',
          'corporateId',
          [sequelize.literal(`(
            select sum("steps") from "challenge_steps"
            where "challengedUserId" in (
              select "id" from "challenge_users" where "challengeId"="challenges"."id"
            ) and "date" = '${moment().tz('Asia/Dubai').subtract(1, 'day').format('YYYY-MM-DD')}'
          )`), 'totalSteps'],
        ],
        include: [{
          model: ChallengeUsers,
          as: 'users',
          where: { leftOn: null },
          attributes: [
            'id',
            'userId',
          ],
        }],
      });
      if (challenges.length) {
        let _promise = [];
        challenges.map((c) => {
          const challenge = c.toJSON();
          if (challenge.users && challenge.users.length) {
            _promise = _promise
              .concat(challenge.users.map((user, i) => createUserNotification({
                userId: user.userId,
                type: constants.notificationTypes.privateChallengeUpdates,
                text: constants.notificationTexts.privateChallengeUpdates,
                relatedChallengeId: challenge.id,
              }, true, false, Math.floor(i / 20), {}, {
                prevSteps: util.numberWithCommas(challenge.totalSteps || 0),
              })));
          }
        });
        let notifications = await Promise.all(_promise);
        notifications = notifications.filter(n => !!n);
        if (notifications.length) {
          await UserNotifications.bulkCreate(notifications, { returning: true });
        }
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeStartingCron: ', e);
    }
  };

  crons.privateChallengeCheck = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          startDate: moment().tz('Asia/Dubai')._d,
          challengeType: constants.challengeTypeLabel.steppi,
          isActive: true,
          isDeleted: false,
          isPrivate: true,
          participants: { [Op.lte]: 1 },
        },
        attributes: [
          'id',
          [sequelize.literal('"challenges"."name"#>>\'{en}\''), 'challengeName'],
          'challengeType',
          'corporateId',
          [sequelize.literal(`(
            select sum("steps") from "challenge_steps"
            where "challengedUserId" in (
              select "id" from "challenge_users" where "challengeId"="challenges"."id"
            ) and "date" = '${moment().tz('Asia/Dubai').subtract(1, 'day').format('YYYY-MM-DD')}'
          )`), 'totalSteps'],
          'createdBy',
        ],
        include: [{
          model: ChallengeUsers,
          as: 'users',
          where: { leftOn: null },
          attributes: [
            'id',
            'userId',
          ],
        }],
      });
      if (challenges.length) {
        const _promise = [];
        const otherPromise = [];
        const updateUserPromise = [];
        challenges.map((c) => {
          const challenge = c.toJSON();
          _promise.push(createUserNotification({
            userId: challenge.createdBy,
            type: constants.notificationTypes.privateChallengeHalt,
            text: constants.notificationTexts.privateChallengeHalt,
            relatedChallengeId: challenge.id,
          }, true, false, 1));
          otherPromise.push(UserChallengeCosts.destroy({
            where: { challengeId: challenge.id, userId: challenge.createdBy },
          }));
          otherPromise.push(Challenges.update({
            isDeleted: true,
          }, {
            where: { id: challenge.id },
          }));
          updateUserPromise.push(Users.update({
            totalSteps: sequelize.literal('get_user_calculated_health_data("users"."id")'),
          }, {
            where: { id: challenge.createdBy },
          }));
        });
        let notifications = await Promise.all(_promise);
        await Promise.all(otherPromise);
        await Promise.all(updateUserPromise);
        notifications = notifications.filter(n => !!n);
        if (notifications.length) {
          await UserNotifications.bulkCreate(notifications, { returning: true });
        }
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeStartingCron: ', e);
    }
  };

  crons.archiveChallenges = async () => {
    try {
      const challenges = await Challenges.findAll({
        where: {
          endDate: { [Op.lte]: moment().tz('Asia/Dubai').subtract(31, 'days')._d },
          isArchived: false,
        },
        attributes: ['id'],
        raw: true,
      });
      if (challenges.length) {
        await Challenges.update({
          isArchived: true,
        }, {
          where: { id: { [Op.in]: challenges.map(c => c.id) } },
        });
      }
    } catch (e) {
      // eslint-disable-next-line
      console.error('error on challengeStartingCron: ', e);
    }
  };

  crons.syncNowNote = () => {
    // eslint-disable-next-line
    new CronJob('20 00 00 * * *', crons.syncNowNoteNotification, null, true, 'Asia/Dubai');
    // eslint-disable-next-line
    new CronJob('00 50 23 * * *', crons.corporateChallengeCompleteNotification, null, true, 'Asia/Dubai');
    // eslint-disable-next-line
    new CronJob('00 05 00 * * *', () => {
      crons.steppiChallengeCompleteNotification();
      crons.privateChallengeCheck();
    }, null, true, 'Asia/Dubai');
    // eslint-disable-next-line
    new CronJob('00 */10 * * * *', () => {
      crons.challengeAnnouncement();
      crons.pc2ChallengeAnnouncement();
    }, null, true, 'Asia/Dubai');
    // eslint-disable-next-line
    new CronJob('00 00 21 * * *', () => {
      crons.corporateChallengeStartingSoonCron();
      crons.publicChallengeStartingSoon();
      crons.privateChallengeStartingSoon();
    }, null, true, 'Asia/Dubai');
    // eslint-disable-next-line
    new CronJob('00 00 08 * * *', () => {
      crons.privateChallengeStarted();
      crons.privateChallengeUpdates();
    }, null, true, 'Asia/Dubai');
    // eslint-disable-next-line
    new CronJob('00 00 00 * * *', crons.archiveChallenges, null, true, 'Asia/Dubai');
    // eslint-disable-next-line
    console.log('cron jobs started');
  };

  return crons;
};
